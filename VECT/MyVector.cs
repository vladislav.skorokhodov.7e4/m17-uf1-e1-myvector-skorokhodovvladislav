﻿using System;

namespace VECT
{
	public class MyVeector
	{
		public int[] puntONE { get; set; }
		public int[] puntTWO { get; set; }

		public MyVeector(int[] pONE, int[] pTWO)
		{
			puntONE = pONE;
			puntTWO = pTWO;
		}


		public double Distancia()
		{
			return Math.Round(Math.Sqrt(Math.Pow((puntTWO[1] - puntONE[1]), 2) + Math.Pow((puntTWO[0] - puntONE[0]), 2)), 2);
		}

		public double Proximitat()
		{
			double compP1 = Math.Round(Math.Sqrt(Math.Pow(puntONE[1], 2) + Math.Pow(puntONE[0], 2)), 2);
			double compP2 = Math.Round(Math.Sqrt(Math.Pow(puntTWO[1], 2) + Math.Pow(puntTWO[0], 2)), 2);

			if (compP1 < compP2)
			{
				return compP1;
			}
			else if (compP2 < compP1)
			{
				return compP2;
			}
			else
			{
				return compP1;
			}
		}

		public void reverse()
		{
			int[] REVpuntONE = puntTWO;
			int[] REVpuntTWO = puntONE;
			puntONE = REVpuntONE;
			puntTWO = REVpuntTWO;
		}

		public string ToString()
		{
			return $"El P1 del vector és ({puntONE[0]}, {puntONE[1]}) i el P2 és ({puntTWO[0]}, {puntTWO[1]}). \nLa llargada és de: {Distancia()}. \nLa distancia entre l'origen és: {Proximitat()}\n";
		}

		public static int CompareLength(MyVeector v1, MyVeector v2)
		{
			return v1.Distancia().CompareTo(v2.Distancia());
		}

		public static int CompareOrigin(MyVeector v1, MyVeector v2)
		{
			return v1.Proximitat().CompareTo(v2.Proximitat());
		}


	}

}
