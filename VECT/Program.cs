﻿using System;

namespace VECT
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Write your name");
            string name = Console.ReadLine();
            Console.WriteLine($"Hello {name} ");

            VectorGame vectorGame = new VectorGame();
            MyVeector[] myVeectors = null;

            int select;
            do
            {
                Console.WriteLine("\nQue vols fer:\n1. Generar Vectors aleatoris \n2. Ordenar vectors \n3. Mostrar vectors \n4. Sortir");
                select = Convert.ToInt32(Console.ReadLine());
                if (select == 1)
                {
                    Console.WriteLine("Introdueix la quantitat dels vectors que vols tenir");
                    myVeectors = vectorGame.randomVectors(Convert.ToInt32(Console.ReadLine()));
                }
                else if (select == 2)
                {
                    Console.WriteLine("\nCom vols Ordenar els vectors:\n1. Per la llargada\n 2. Per la distancia entre entre l'origen");
                    int chose = Convert.ToInt32(Console.ReadLine());
                    vectorGame.SortVectors(myVeectors, chose);
                }
                else if (select == 3)
                {
                    if (myVeectors == null)
                    {
                        Console.WriteLine("ERROR, no hi han vectors");
                    }
                    else
                    {
                        vectorGame.mostraArray(myVeectors);
                    }
                }
            } while (select != 4);  
        }
    }
}

