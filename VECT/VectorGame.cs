﻿using System;

namespace VECT
{
    public class VectorGame
    {
        public MyVeector[] randomVectors(int num)
        {
            Random rand = new Random();

            MyVeector[] myVeectors = new MyVeector[num];
            for (int i = 0; i < myVeectors.Length; i++)
            {
                int[] rrr = { rand.Next(-100, 100), rand.Next(-100, 100) };
                int[] ttt = { rand.Next(-100, 100), rand.Next(-100, 100) };
                myVeectors[i] = new MyVeector(rrr, ttt);
            }
            return myVeectors;
        }

        public MyVeector[] SortVectors(MyVeector[] myVeectors, int chose)
        {
            mostraArray(myVeectors);
            if (chose == 1)
            {
                Array.Sort(myVeectors, MyVeector.CompareLength);
            }
            else if (chose == 2)
            {
                Array.Sort(myVeectors, MyVeector.CompareOrigin);
            }
            mostraArray(myVeectors);
            return myVeectors;

        }
        public void mostraArray(MyVeector[] myVeectors)
        {
            foreach (var item in myVeectors)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }

}
